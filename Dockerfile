FROM hiromuhota/webspoon

MAINTAINER contact@sigent.fr

ARG DEBIAN_FRONTEND=noninteractive

ENV K_LANGUAGE=fr_FR
ENV K_TIMEZONE="Europe/Paris"

ENV K_UMASK="0027"
ENV K_PKG="gdal-bin imagemagick rename sqlite3"
ENV K_JARS_URL=https://bitbucket.org/sigent/webspoon/raw/master/updateJARS.sh

USER root

RUN apt-get update \
	&& apt-get install -y \
		locales \
		${K_PKG} \
	&& apt-get clean \
	&& rm -r /var/lib/apt/lists/*

RUN localedef -c -f UTF-8 -i ${K_LANGUAGE} ${K_LANGUAGE}.utf8 \
	&& echo ${K_TIMEZONE} > /etc/timezone

RUN sed -i 's/UMASK="0027"/UMASK=${K_UMASK}/' ${CATALINA_HOME}/bin/catalina.sh

USER tomcat

ADD --chown=tomcat:tomcat ${K_JARS_URL} /tmp/
RUN sh /tmp/updateJARS.sh

RUN mkdir -p $HOME/share

ENV LANG=fr_FR.utf8
ENV LC_ALL=fr_FR.utf8